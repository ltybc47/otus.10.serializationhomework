﻿using LibFromEducation;
using LibFromEducation.Classes;
using LibFromEducation.Interfaces;
using LibFromEducation.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChatServer
{
  public static class GlobalVars
  {
    public static IniFile INI = new IniFile(AppContext.BaseDirectory + @"\config.ini");
    public static IStorage<User> Users = new FileStorage<User>(fileName: "User.csv", new TestCsvHelperSerializer());
    public static TestJsonSerializer JsonSerializer = new TestJsonSerializer();
  }
}
