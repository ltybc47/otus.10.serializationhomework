﻿using LibFromEducation;
using LibFromEducation.Classes;
using LibFromEducation.Serialization;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using Serilog;

namespace ChatServer
{
  class ClientObject
  {
    public User user;
    protected internal string Id { get; private set; }
    protected internal NetworkStream Stream { get; private set; }
    TcpClient client;
    ServerObject server; // объект сервера

    public ClientObject(TcpClient tcpClient, ServerObject serverObject)
    {
      Id = Guid.NewGuid().ToString();
      client = tcpClient;
      server = serverObject;
      serverObject.AddConnection(this);
    }

    public void Process()
    {
      
      try
      {
        Stream = client.GetStream();
        // получаем имя пользователя
        Message msg = GlobalVars.JsonSerializer.Deserialize<Message>(GetMessage());

        string userName = msg.Msg.Split(" ")[0];
        if (!string.IsNullOrEmpty(userName))
        {
          user = GlobalVars.Users.GetFirstByProperty(propertyName: "Name", Value: userName);
          if (user is null)
          {
            user = new User() { Name = userName };
            GlobalVars.Users.Add(user);
          } 
          // посылаем сообщение о входе в чат всем подключенным пользователям
          server.BroadcastMessage(new Message(user.Name + " вошел в чат"), this.Id);
          Log.Information(msg.Msg);
          // в бесконечном цикле получаем сообщения от клиента
          while (true)
          {
            try
            {
              string message = GetMessage();
              msg = GlobalVars.JsonSerializer.Deserialize<Message>(message);
              msg.Msg = String.Format("{0}: {1}", userName, msg.Msg);
              Log.Information(msg.Msg);
              server.BroadcastMessage(msg, this.Id);
            }
            catch (Exception ex)
            {
              Log.Error(ex.Message);
              Log.Error(ex.Source);
              msg.Msg = String.Format("{0}: logout", userName);
              Log.Information(msg.Msg);
              server.BroadcastMessage(msg, this.Id);
              break;
            }
          }
        }
        else
        {
          Log.Error("Error logging");
        }

      }
      catch (Exception e)
      {
        Log.Error(e.Message);
      }
      finally
      {
        // в случае выхода из цикла закрываем ресурсы
        server.RemoveConnection(this.Id);
        Close();
      }
    }

    // чтение входящего сообщения и преобразование в строку
    private string GetMessage()
    {
      byte[] data = new byte[64]; // буфер для получаемых данных
      StringBuilder builder = new StringBuilder();
      int bytes = 0;
      do
      {
        bytes = Stream.Read(data, 0, data.Length);
        builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
      }
      while (Stream.DataAvailable);

      return builder.ToString();
    }

    // закрытие подключения
    protected internal void Close()
    {
      if (Stream != null)
        Stream.Close();
      if (client != null)
        client.Close();
    }
  }
}
