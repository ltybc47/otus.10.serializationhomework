﻿using LibFromEducation;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace ChatServer
{
  class ServerObject
  {
    static TcpListener tcpListenerPrimary; // сервер для прослушивания
    static TcpListener tcpListenerStandby; // сервер для прослушивания
    List<ClientObject> clients = new List<ClientObject>(); // все подключения

    protected internal void AddConnection(ClientObject clientObject)
    {
      clients.Add(clientObject);
    }
    protected internal void RemoveConnection(string id)
    {
      // получаем по id закрытое подключение
      ClientObject client = clients.FirstOrDefault(c => c.Id == id);
      // и удаляем его из списка подключений
      if (client != null)
        clients.Remove(client);
    }
    // прослушивание входящих подключений
    protected internal void Listen()
    {
      try
      {
        tcpListenerPrimary = new TcpListener(IPAddress.Any, Convert.ToInt32(GlobalVars.INI.GetPrivateString("main", "PrimaryPort")));
        tcpListenerPrimary.Start();
        Log.Information("Сервер запущен. Ожидание подключений...");

        while (true)
        {
          TcpClient tcpClient = tcpListenerPrimary.AcceptTcpClient();

          ClientObject clientObject = new ClientObject(tcpClient, this);
          Thread clientThread = new Thread(new ThreadStart(clientObject.Process));
          clientThread.Start();
        }
      }
      catch (Exception ex)
      {
        Log.Error(ex.Message);
        Disconnect();
      }
    }

    // трансляция сообщения подключенным клиентам
    protected internal void BroadcastMessage(Message message, string id)
    {
      byte[] data = Encoding.Unicode.GetBytes(GlobalVars.JsonSerializer.Serialize(message));
      for (int i = 0; i < clients.Count; i++)
      {
        if (clients[i].Id != id) // если id клиента не равно id отправляющего
        {
          clients[i].Stream.Write(data, 0, data.Length); //передача данных
        }
      }
    }
    // отключение всех клиентов
    protected internal void Disconnect()
    {
      tcpListenerPrimary.Stop(); //остановка сервера

      for (int i = 0; i < clients.Count; i++)
      {
        clients[i].Close(); //отключение клиента
      }
      Environment.Exit(0); //завершение процесса
    }
  }
}
