﻿using System;
using System.Threading;
using LibFromEducation;
using Serilog;
using Serilog.Events;

namespace ChatServer
{
  class Program
  {
    static ServerObject server; // сервер
    static Thread listenThread; // потока для прослушивания
    
    static void Main(string[] args)
    {
      Log.Logger = new LoggerConfiguration()
        .MinimumLevel.Debug()
        //.MinimumLevel.Override("Microsoft", LogEventLevel.Information)
        //.MinimumLevel.Override("Microsoft.AspNetCore", LogEventLevel.Warning)
        .Enrich.FromLogContext()
        .WriteTo.Console()
        .WriteTo.File("log.txt", rollingInterval: RollingInterval.Hour)
        .CreateLogger();
      Log.Information("Init");
      try
      {
        GlobalVars.INI.WritePrivateString("main", "PrimaryPort", 8888.ToString());
        GlobalVars.INI.WritePrivateString("main", "SecondaryPort", 8888.ToString());

        server = new ServerObject();
        listenThread = new Thread(new ThreadStart(server.Listen));
        listenThread.Start(); //старт потока
      }
      catch (Exception ex)
      {
        server.Disconnect(); 
        Log.Error(ex.Message);
      }
    }
  }
}
