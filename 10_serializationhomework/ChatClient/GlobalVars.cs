﻿using LibFromEducation;
using LibFromEducation.Serialization;
using System;

namespace ChatClient
{
  class GlobalVars
  {
    public static IniFile INI = new IniFile(AppContext.BaseDirectory + @"\config.ini");
    public static TestJsonSerializer JsonSerializer = new TestJsonSerializer();
  }
}
