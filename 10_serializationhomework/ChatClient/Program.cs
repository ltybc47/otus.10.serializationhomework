﻿using LibFromEducation;
using LibFromEducation.Enum;
using Serilog;
using System;
using System.Text;
using System.Threading;

namespace ChatClient
{
	class Program
	{
		static void Main(string[] args)
		{

			Log.Logger = new LoggerConfiguration()
			.MinimumLevel.Debug()
			//.MinimumLevel.Override("Microsoft", LogEventLevel.Information)
			//.MinimumLevel.Override("Microsoft.AspNetCore", LogEventLevel.Warning)
			.Enrich.FromLogContext()
			.WriteTo.File("log.txt", rollingInterval: RollingInterval.Day)
			.CreateLogger();


			Client.userName = GlobalVars.INI.GetPrivateString("User", "Name");
			if (string.IsNullOrEmpty(Client.userName))
			{
				Console.Write("Введите свое имя: ");
				Client.userName = Console.ReadLine();
				if (!string.IsNullOrEmpty(Client.userName)) GlobalVars.INI.WritePrivateString("User", "Name", Client.userName);
			}

			try
			{
				if (Client.ConnectToServer())
				{
					Message msg = Client.CreateMessage(message: $"{Client.userName} loging", from: 0, status: ClientStatus.LogIn);
					byte[] data = Encoding.Unicode.GetBytes(GlobalVars.JsonSerializer.Serialize(msg));
					Client.stream.Write(data, 0, data.Length);

					// запускаем новый поток для получения данных
					Thread receiveThread = new Thread(new ThreadStart(Client.ReceiveMessage));
					receiveThread.Start(); //старт потока
					Console.WriteLine("Добро пожаловать, {0}", Client.userName);
					Client.SendMessage();
				}
				else
				{
					string msg = "Ошибка подключения к серверу";
					Log.Error(msg);
					Console.WriteLine(msg);
				}
			}
			catch (Exception ex)
			{
				Log.Error(ex.Message);
				Console.WriteLine(ex.Message);
			}
			finally
			{
				Client.Disconnect();
			}
		}
	}
}
