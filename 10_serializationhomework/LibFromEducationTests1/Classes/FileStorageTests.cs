﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using LibFromEducation.Classes;
using System;
using System.Collections.Generic;
using System.Text;
using LibFromEducation.Interfaces;
using LibFromEducation.Serialization;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using CsvHelper;
using System.Globalization;

namespace LibFromEducation.Classes.Tests
{
  [TestClass()]
  public class FileStorageTests
  {
    [TestMethod()]
    public void TestCsvSerializer_SaveReadFile_Test()
    {
      try
      {
        Console.WriteLine(2 + 2);
        TestCsvHelperSerializer csv = new TestCsvHelperSerializer();
        User user = new User() { Guid = "GUID", Name = "Igor" };
        csv.SaveToFile<User>(user, "User.csv");
        IEnumerable<User> users = csv.DeserializeFile<User>("User.csv");
        Console.WriteLine(string.Join("\n", users.Select(x => ((x as User).Name + " " + (x as User).Guid))));
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        Console.WriteLine(e.StackTrace);
        Assert.Fail();
      }
    }

    [TestMethod()]
    public void TestCsvSerializer_SaveFile_Test()
    {
      try
      {
        Console.WriteLine(2 + 2);
        TestCsvHelperSerializer csv = new TestCsvHelperSerializer();
        User user = new User() { Guid = "GUID", Name = "Igor" };
        csv.SaveToFile<User>(user, "User.csv");
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        Console.WriteLine(e.StackTrace);
        Assert.Fail();
      }
    }

    [TestMethod()]
    public void TestCsvSerializer_ReadFile_Test()
    {
      try
      {
        Console.WriteLine(2 + 2);
        TestCsvHelperSerializer csv = new TestCsvHelperSerializer();
        //Console.WriteLine(JsonConvert.SerializeObject(csv.ReadFile<User>("User.csv")));
        List<User> users = (csv.DeserializeFile<User>("User.csv").ToList());
        Console.WriteLine(string.Join("\n", users.Select(x => ((x as User).Name + " " + (x as User).Guid))));
        //Console.WriteLine(string.Join("\n", users.Select(x => ((x as User).Name + " " + (x as User).Guid))));
        //Console.WriteLine(JsonConvert.SerializeObject(users));
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        Console.WriteLine(e.StackTrace);
        Assert.Fail();
      }
    }

    [TestMethod()]
    public void String_DeSerialization_CSV_Test()
    {
      string foo = "1,one\n\r2,tho";
      TestCsvSerializer csv = new TestCsvSerializer();
      Console.WriteLine(JsonConvert.SerializeObject(csv.Deserialize<User>(foo)));
    }

    [TestMethod()]
    public void String_Serialize_DeSerialization_CSV_Test()
    {
      TestCsvSerializer csv = new TestCsvSerializer();
      Console.WriteLine(csv.Deserialize<User>(csv.Serialize<User>(new User() { Guid = "ho", Name = "jim" })).ToList()[0].ToString());
    }

    [TestMethod()]
    public void GetAllTest()
    {

      Assert.Fail();
    }
  }
}