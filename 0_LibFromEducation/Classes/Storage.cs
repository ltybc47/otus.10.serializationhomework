﻿using LibFromEducation.Interfaces;
using LibFromEducation.Serialization;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace LibFromEducation
{
  /// <summary>
  /// Типо склад фиксированного размера
  /// </summary>
  public class Storage : Box,  IEnumerable, IAlgoritm
  {
    const int defSize = 1000;
    private ISerializerLib _serialize;
    public Storage()
    {
      X = Y = Z = defSize;
    }

    public new IEnumerator GetEnumerator()
    {
      for (int i = 0; i < Content.Count; i++)
      {
        yield return Content[i];
      }
    }

    //public IEnumerable GetContents()
    
    public override void Save()
    {
      var state = _serialize.Serialize(Content);
      File.WriteAllText("./Container.state", state);
    }

    public void Sort()
    {
      Content.Sort(SortBoxBySize);
    }
    //public static int SortBoxBySize(Box b1, Box b2)
    //{
    //  return b2.V - b1.V;
    //  //return Convert.ToInt32(b1.V > b2.V);
    //}
  }
}