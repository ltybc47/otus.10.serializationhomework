﻿using LibFromEducation.Serialization;
using System;
using System.Collections;
using System.IO;

namespace LibFromEducation
{
  public class Container : Box
  {
    #region property
    private ISerializerLib _serialize;

    /// <summary>
    /// двумерный массив высот представленных в виде битового массива, воспринимать как X,Y,Z
    /// </summary>
    public BitArray[,] Volume { get; set; }
    #endregion
    #region Constructor

    public Container()
    {
      X = 20;
      Y = 20;
      Z = 20;
      Volume = new BitArray[X, Y];
      for (int x = 0; x < X; x++)
      {
        for (int y = 0; y < Y; y++)
        {
          Volume[x, y] = new BitArray(Z);
        }
      }
    }

    #endregion

    #region Methods
    public void Save()
    {
      var state = _serialize.Serialize(Content);
      File.WriteAllText("./Container.state", state);
    }
    #endregion
    public void View()
    {
      int deltaX = 3;
      int deltaY = 3;
      for (int i = 0; i < X; i++)
      {
        for (int j = 0; j < Y; j++)
        {
          Console.SetCursorPosition(i + deltaX, j + deltaY);
          int CountHeight = 0;
          if (Volume[i, j].Get(0) == true)
          {
            while (Volume[i, j].Get(CountHeight) == true)
            {
              CountHeight++;
            }
          }
          Console.Write(CountHeight);
        }
      }
    }
    /// <summary>
    /// Проверка на всовываемость, уместиться ли в свободном пространстве коробочка
    /// </summary>
    /// <param name="b">Коробка, при успешном поиске свободного пространства присваивает координаты в которые можно ставить коробку</param>
    /// <returns></returns>
    public bool tryAdd(Box b)
    {
      b.FlipLowSize();
      if (b.X <= X & b.Y <= Y & b.Z <= Z)                      // если коробка вообще вмещается
      {
        //ищем пустоту по X,Y

        for (int x = 0; x < X; x++)
        {
          for (int y = 0; y < Y; y++)
          {

            if (Volume[x, y].Get(0) == false            // если мы нашупали пустоту 
                && X - x >= b.X                            // и по X мы можем воткнуть коробку предустматривается оценка уже заполненного не до конца пространства
                && Y - y >= b.Y                           // и по Y мы можем воткнуть коробку
                )
            {
              b.Coordinate.Set(x, y, 0);
              return true;
            }
          }
        }
      }
      return false;
    }
    /// <summary>
    /// Позволяет добавлять в контейнер коробки
    /// </summary>
    /// <param name="c">container</param>
    /// <param name="b">box</param>
    /// <returns></returns>
    public static Container operator +(Container c, Box b)
    {
      //добавляем коробочку
      for (int bx = 0; bx < b.X; bx++)
      {
        for (int by = 0; by < b.Y; by++)
        {
          for (int bz = 0; bz < b.Z; bz++)
          {
            c.Volume[b.Coordinate.X + bx, b.Coordinate.Y + by].Set(bz, true);
          }
        }
      }
      c.Content.Add(b);
      return c;
    }
    public IEnumerator GetEnumerator()
    {
      return Content.GetEnumerator();
    }
    public Box this[int index]
    {
      get { return Content[index]; }
      set { Content[index] = value; }
    }


  }
}