﻿using LibFromEducation.Interfaces;
using LibFromEducation.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace LibFromEducation
{
  public class Message : IMessage
  {
    [NonSerialized]
    private string typeFormat;

    public int IdFromUser { get; set; }
    public int IdToUser { get; set; }
    public string Msg { get; set; }
    public Exception Exception { get; set; }
    public DateTime DateTime { get; set; } = DateTime.Now;
    TestJsonSerializer testJsonSerializer = new TestJsonSerializer();

    public LibFromEducation.Enum.ClientStatus ClientStatus = LibFromEducation.Enum.ClientStatus.None;

    public string TypeFormat { get => typeFormat; set => typeFormat = value; }

    public Message()
    {

    }
    public Message(string message, int from, int to)
    {
      typeFormat = "json";
      IdFromUser = from;
      IdToUser = to;
      Msg = message;
    }

    public Message(string message)
    {
      typeFormat = "json";
      Msg = message;
    }
  }
}
