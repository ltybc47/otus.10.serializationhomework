﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibFromEducation
{
  public class Coordinate
  {
    public Coordinate()
    {
      //X = Y = Z = 0;
    }
    public int X { get; set; }
    public int Y { get; set; }
    public int Z { get; set; }

    internal void Set(int x, int y, int z)
    {
      X = x;
      Y = y;
      Z = z;
    }
  }

}
