﻿using LibFromEducation.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LibFromEducation
{
  [Serializable]
  public class Box 
    //: IEnumerable
  {
    #region Properties

    /// <summary>
    /// length 
    /// </summary>
    public int X { get; set; }
    /// <summary>
    /// width
    /// </summary>
    public int Y { get; set; }
    /// <summary>
    /// heigth
    /// </summary>
    public int Z { get; set; }
    /// <summary>
    /// volume
    /// </summary>
    public int V { get { return X * Y * Z; } }
    /// <summary>
    /// Масса
    /// </summary>
    public int weight { get; set; }
    public Coordinate Coordinate { get; set; } = new Coordinate();
    /// <summary>
    /// Содержимое коробки
    /// </summary>
    public List<Box> Content = new List<Box>();
    public override string ToString() => $"({X}),({Y}),({Z}), V - {V});";

    #endregion
    private readonly ISerializerLib _serializer;
    #region Constructors
    public Box()
    {


    }
    /// <summary>
    /// Cube Constructor
    /// </summary>
    /// <param name="v">size</param>
    public Box(int v)
    {
      if (v == 0)
        throw new System.FormatException("Zero parameter side");
      X = Y = Z = v;
    }
    /// <summary>
    /// Box Constructor
    /// </summary>
    /// <param name="x">X</param>
    /// <param name="y">Y</param>
    /// <param name="z">Z</param>
    public Box(int x, int y, int z)
    {
      if (x == 0 || y == 0 || z == 0)
        throw new System.FormatException("Zero parameter side");
      X = x;
      Y = y;
      Z = z;
    }

    #endregion

    #region Methods

    public virtual void Save()
    {
      var state = _serializer.Serialize(this);
      File.WriteAllText("./game.state", state);
    }

    /// <summary>
    /// Box Constructor
    /// </summary>
    /// <param name="b1"> first box</param>
    /// <param name="b2">Second box</param>
    /// <returns></returns>
    public static Box operator +(Box b1, Box b2)
    {
      Box resultbox = new Box(b1.X + b2.X, b1.Y + b2.Y, b1.Z + b2.Z);// во на это безобразие будет плодить много мусора в куче.
      resultbox.Content.AddRange(new List<Box>() { b1, b2 });
      return resultbox;
    }
    public static bool operator ==(Box b1, Box b2)
    {
      b1.FlipLowSize();
      b2.FlipLowSize();
      if (b1.X == b2.X && b1.Y == b2.Y && b1.Z == b2.Z)
        return true;
      else
        return false;
    }
    public static bool operator !=(Box b1, Box b2)
    {

      b1.FlipLowSize();
      b2.FlipLowSize();
      if (b1.X == b2.X && b1.Y == b2.Y && b1.Z == b2.Z)
        return false;
      else
        return true;
    }
    /// <summary>
    /// Провернуть вдоль оси X
    /// </summary>
    public void RotateX()
    {
      int buf = Y;
      Y = Z;
      Z = buf;
    }
    /// <summary>
    /// Провернуть вдоль оси Y
    /// </summary>
    public void RotateY()
    {
      int buf = X;
      X = Z;
      Z = buf;
    }
    /// <summary>
    /// Провернуть вдоль оси X
    /// </summary>
    public void RotateZ()
    {
      int buf = Y;
      Y = X;
      X = buf;
    }
    /// <summary>
    /// повернуть коробку так чтобы в высоту занимала меньше пространства, в ширину больше пространства, в глубину среднее
    /// </summary>
    internal void FlipLowSize()
    {
      if (Z > X)
        RotateY();

      if (Z > Y)
        RotateX();

      if (Y > X)
        RotateZ();

    }
    public static int SortBoxBySize(Box b1, Box b2)
    {
      return b2.V - b1.V;
      //return Convert.ToInt32(b1.V > b2.V);
    }

    //public IEnumerator<Box> GetEnumerator()
    //{
    //  for (int i = 0; i < Content.Count; i++)
    //  {
    //    yield return Content[i];
    //  }
    //}
    public override bool Equals(object obj)
    {

      return obj is Box box &&
             X == box.X &&
             Y == box.Y &&
             Z == box.Z &&
             V == box.V &&
             weight == box.weight;
    }

    //IEnumerator IEnumerable.GetEnumerator()
    //{
    //  return GetEnumerator();
    //}


    #endregion
  }

  internal class BoxEnumerator : IEnumerator<Box>
  {
    private List<Box> contents;
    int position = -1;
    public BoxEnumerator(List<Box> contents)
    {
      this.contents = contents;
    }

    public Box Current
    {
      get
      {
        if (position == -1 || position >= contents.Count)
          throw new InvalidOperationException();
        return contents[position];
      }
    }

    object IEnumerator.Current => contents[position];

    public void Dispose()
    {
    }

    public bool MoveNext()
    {
      if (position < contents.Count - 1)
      {
        position++;
        return true;
      }
      else
        return false;
    }

    public void Reset()
    {
      position = -1;
    }
  }
}
