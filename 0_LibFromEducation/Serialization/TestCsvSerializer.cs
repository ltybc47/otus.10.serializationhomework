﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace LibFromEducation.Serialization
{
  public class TestCsvSerializer : ISerializerLib
  {
    //нам же нужно создать экземпляры типа Т, подойдет такой подход?
    public IEnumerable<T> Deserialize<T>(string s) where T : new()
    {
      List< T > items = new List<T>();
      foreach (string line in s.Split("\r\n").ToList())     // работать будет как для однострочных так и для многострочных строк
      {
        // инициализируем пустой экземпляр
        var data = new T();
        string[] arrline = line.Split(';');
        int cursor = 0;
        if (arrline.Length == data.GetType().GetFields().Count())
        {
          foreach (FieldInfo field in data.GetType().GetFields())
          {
            if (field.FieldType == typeof(System.String))
              data.GetType().GetField(field.Name).SetValue(data, arrline[cursor].ToString());
            else if (field.FieldType == typeof(Int32))
              data.GetType().GetField(field.Name).SetValue(data, Convert.ToInt32(arrline[cursor]));
            else
              throw new Exception($"Не найдено соответствие к типу {field.FieldType} у поля {field.Name}");
            //.... и так по всем типам, чтобы корректно обработать конвертацию по всем типам
            cursor++;
          }
          //Console.WriteLine(string.Join(";", data.GetType().GetFields().Select(x => x.GetValue(data)).ToList()));
        }
        items.Add(data);
      }
      return items;
    }

    public void Dispose()
    {
      
    }

    public string Serialize<T>(T item)
    {
      return string.Join(";", item.GetType().GetFields().Select(x => x.GetValue(item)).ToList()) + "\r\n";
    }

    public string Serialize<T>(List<T> items)
    {
      string answer = "";
      foreach (var item in items)
      {
        answer += string.Join(";", item.GetType().GetFields().Select(x => x.GetValue(item)).ToList()) + "\r\n";
      }
      return answer;
    }
  }
}
