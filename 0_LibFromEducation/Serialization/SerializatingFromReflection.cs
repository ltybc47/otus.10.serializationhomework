﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LibFromEducation.Serialization
{
  class SerializatingFromReflection
  {
    private static string Serializate(object d)
    {
      string methods = string.Join("\n", d.GetType().GetMethods().Select(x => x.Name.ToString()).ToList());
      string fields = string.Join("\n", d.GetType().GetFields().Select(x => x.Name.ToString() + " - " + x.GetValue(d).ToString()).ToList());
      return methods + "\n" + fields;
    }
  }
}
