﻿using CsvHelper;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace LibFromEducation.Serialization
{
  public class TestCsvHelperSerializer : ISerializerLib
  {
    public TestCsvHelperSerializer()
    {
      //using (File.Create("storage.csv"))
      //{

      //}
    }

    public IEnumerable<T> DeserializeFile<T>(string fileName)
    {
      var csv = new CsvReader(new StreamReader(fileName), CultureInfo.InvariantCulture);
      csv.Configuration.HasHeaderRecord = false;
      var records = csv.GetRecords<T>();
      return records.ToList();
    }
    public IEnumerable<T> Deserialize<T>(string v) where T : new()
    {

      TextReader reader = new StringReader(v);
      var csv = new CsvReader(reader, CultureInfo.InvariantCulture);
      csv.Configuration.HasHeaderRecord = false;
      return csv.GetRecords<T>().ToList();
    }
    public string Serialize<T>(T item)
    {
      //using (var writer = new StreamWriter("storage.csv", true))

      using (var stream = new MemoryStream())
      using (var reader = new StreamReader(stream))
      using (var writer = new StreamWriter(stream))
      using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
      {
        csv.Configuration.HasHeaderRecord = false;
        csv.WriteRecords(new List<T>() { item });
        writer.Flush();
        stream.Position = 0;
        return reader.ReadToEnd();
      }
    }
    public string Serialize<T>(List<T> items)
    {
      //using (var writer = new StreamWriter("storage.csv", true))
      using (var stream = new MemoryStream())
      using (var reader = new StreamReader(stream))
      using (var writer = new StreamWriter(stream))
      using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
      {
        csv.Configuration.HasHeaderRecord = false;
        csv.WriteRecords(items);
        writer.Flush();
        stream.Position = 0;
        return reader.ReadToEnd();
      }
    }
    /// <summary>
    /// save List objects
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="obj"></param>
    /// <param name="fileName"></param>
    /// <returns></returns>
    public bool SaveToFile<T>(List<T> obj, string fileName)
    {
      try
      {
        using (var streamWriter = new StreamWriter(fileName))
        using (var csv = new CsvWriter(streamWriter, CultureInfo.InvariantCulture))
        {
          csv.WriteRecords(obj);
          return true;
        }
      }
      catch (Exception)
      {
        //todo make Log method
        return false;
      }

    }
    /// <summary>
    /// save One object
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="obj"></param>
    /// <param name="fileName"></param>
    /// <returns></returns>
    public bool SaveToFile<T>(T obj, string fileName)
    {
      try
      {
        using (var streamWriter = new StreamWriter(fileName))
        using (var csv = new CsvWriter(streamWriter, CultureInfo.InvariantCulture))
        {
          csv.WriteRecord(obj);
          return true;
        }
      }
      catch (Exception)
      {
        //todo make Log method
        return false;
      }

    }

  }
}
