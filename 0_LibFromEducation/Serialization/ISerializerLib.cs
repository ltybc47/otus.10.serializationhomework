﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace LibFromEducation.Serialization
{
  public interface ISerializerLib
  {
    string Serialize<T>(T item);
    IEnumerable<T> Deserialize<T>(string s) where T : new();
  }

}
