﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace LibFromEducation.Serialization
{
  public class TestJsonSerializer : ISerializerLib
  {
    public T Deserialize<T>(string v)
    {
      return JsonConvert.DeserializeObject<T>(v);
    }

    public IEnumerable<T> DeserializeList<T>(string v)
    {
      return JsonConvert.DeserializeObject<List<T>>(v);
    }

    public string Serialize<T>(T item)
    {
      return JsonConvert.SerializeObject(item);
    }

    IEnumerable<T> ISerializerLib.Deserialize<T>(string s)
    {
      return DeserializeList<T>(s);
    }
  }
}
